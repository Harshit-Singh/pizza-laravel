# Pizza Odering Web Application

![image](https://drive.google.com/uc?export=view&id=17BUnRRwevXo8s76nsa-_XB38_D5W1yp3)

## Demo [Demo](https://pizzalove.herokuapp.com)
### Login Credentials for Demo are - 
- for admin
    - email - admin@pizza.com
    - password - password
- for user
    - email - testuser@pizza.com
    - password - password
    - You may  also create new user account by Signing up and Registering
      
## List of Featues
1. Order Online Without an Account
2. User can Sign Up and Login to have a track your order history
3. Separate Dashbords for -
    * User 
    * Admin / Operator 
    * Guest User
  
4. Modern Async, Interactive and responive UI, For example, 
    - The cart badge number update on make changes in to the cart 
    - Color and text of "Add to cart" button change when the item is added to the cart to "update quantity" and Orange
5. Make Multiple Selection of Pizzas with varying quantities, this is then processed as a single order
6. Custom Login and Registration Pages and Controllers
7. Automatic Calculation of Bill with and without Delivery Charges
8. Custom and Informative Success and Error Messages repective events
9. Review Order in Slider before making final Proceed to Pay, 
10. Routes Protection using custom Middleware

## Here is a list of some Extra Features Available to Different Users in Dashboads

1.  Admin
    - has access to view the list of all orders made in a table
    - can View All Pizza or Add New Pizza Items to the Pizza Menu (Work in progress for Update and Delete (CRUD) )
    - has access to view the list of registered users

2.  User
    - Can order from his/her dashboard, the Id of the user uniquely identify the order made by user
    - View the history of all the orders made by him/her
    - Profile Button is available to create User Profile (Yet to be made available)
3. Guest
    - Guest User can order directly from the home page which makes ordering super simple

## Demo Screenshots

![image](https://drive.google.com/uc?export=view&id=1l2DQbcb2F_vuEdOOHNL6ss4i_DNxB5kX)

![image](https://drive.google.com/uc?export=view&id=1ZVd0pNGc7irNh8oB6XuhXKk5hC5Z0phR)


![image](https://drive.google.com/uc?export=view&id=1rY-YGOBHKUWvOe6-zqedwEbm0H1RLpsS)


![image](https://drive.google.com/uc?export=view&id=1PF9scLiRd0xXiHGdM7AULyNzyxZ_g_Y6)


![image](https://drive.google.com/uc?export=view&id=1TF3fqx41EfP5q6cwoLDwXZrSUwxSA9FM)


![image](https://drive.google.com/uc?export=view&id=1CqsdXye1dgD6AnMWht4Ka9etM0u2bWjf)


![image](https://drive.google.com/uc?export=view&id=1KVposO0VfE9g-su19I1s4AREOUS49Rd0)


![image](https://drive.google.com/uc?export=view&id=1sJ3RzeInGJVXjakNrXQ67mHwq84uYvHe)


#### Note This is only a demo application not a service


## Technologies Used 
1. Laravel/PHP
2. VueJS, Vuetify
3. remotemysql
4. Heroku 







